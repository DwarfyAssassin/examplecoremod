package io.gitlab.dwarfyassassin.examplecoremod;

import java.util.Map;
import org.apache.logging.log4j.LogManager;
import cpw.mods.fml.relauncher.IFMLCallHook;
import io.gitlab.dwarfyassassin.lotrucp.core.UCPCoreMod;

public class ECMCoreSetup implements IFMLCallHook {

    @Override
    public Void call() throws Exception {
        ECMCoreMod.log = LogManager.getLogger("ECM");

        UCPCoreMod.registerPatcher(new MCPatcher());

        return null;
    }

    @Override
    public void injectData(Map<String, Object> data) {
    }
}
