package io.gitlab.dwarfyassassin.examplecoremod;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.IntInsnNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;
import cpw.mods.fml.common.asm.transformers.deobf.FMLDeobfuscatingRemapper;
import io.gitlab.dwarfyassassin.lotrucp.core.patches.base.Patcher;
import io.gitlab.dwarfyassassin.lotrucp.core.utils.ASMUtils;
import net.minecraftforge.classloading.FMLForgePlugin;

public class MCPatcher extends Patcher {

    public MCPatcher() {
        super("MC");
        this.classes.put("net.minecraft.server.management.ServerConfigurationManager", (classNode) -> patchSendPlayerListPackets(classNode));
        this.classes.put("net.minecraft.network.play.server.S38PacketPlayerListItem", (classNode) -> patchUsernameLength(classNode));
    }
    
    @Override
    public boolean canRun(String className) {
        className = FMLForgePlugin.RUNTIME_DEOBF ? FMLDeobfuscatingRemapper.INSTANCE.map(className).replace("/", ".") : className;
        
        return super.canRun(className);
    }

    @Override
    public void run(String className, ClassNode classNode) {
        className = FMLForgePlugin.RUNTIME_DEOBF ? FMLDeobfuscatingRemapper.INSTANCE.map(className).replace("/", ".") : className;
        
        this.classes.get(className).accept(classNode); 
        this.classes.remove(className);
    }

    private void patchUsernameLength(ClassNode classNode) {
        MethodNode method = ASMUtils.findMethod(classNode, " readPacketData", "func_148837_a", "(Lnet/minecraft/network/PacketBuffer;)V");
        if(method == null) return;

        for(AbstractInsnNode node : method.instructions.toArray()) {
            if(node instanceof IntInsnNode) {
                IntInsnNode intNode = (IntInsnNode) node;
                if(intNode.operand != 16) continue;
                
                intNode.operand = 32;
                ECMCoreMod.log.info("Patched max username length..");
                break;
            }
        }
    }
    
    private void patchSendPlayerListPackets(ClassNode classNode) {
        MethodNode method_logIn = ASMUtils.findMethod(classNode, "playerLoggedIn", "func_72377_c", "(Lnet/minecraft/entity/player/EntityPlayerMP;)V");
        if(method_logIn != null) patchGetCommandSenderName(method_logIn);

        MethodNode method_logOut = ASMUtils.findMethod(classNode, "playerLoggedOut", "func_72367_e", "(Lnet/minecraft/entity/player/EntityPlayerMP;)V");
        if(method_logOut != null) patchGetCommandSenderName(method_logOut);
        
        MethodNode method_sendInfo = ASMUtils.findMethod(classNode, "sendPlayerInfoToAllPlayers", "func_72374_b", "()V");
        if(method_sendInfo != null) patchGetCommandSenderName(method_sendInfo);
        
        ECMCoreMod.log.info("Patched sendplayer list packets.");
    }
    
    private void patchGetCommandSenderName(MethodNode method) {
        for(AbstractInsnNode node : method.instructions.toArray()) {
            if(node instanceof MethodInsnNode) {
                MethodInsnNode methodNode = (MethodInsnNode) node;
                if(!methodNode.name.equals("getCommandSenderName") && !methodNode.name.equals("func_70005_c_")) continue;
                
                method.instructions.insert(methodNode, new MethodInsnNode(Opcodes.INVOKESTATIC, "io/gitlab/dwarfyassassin/examplecoremod/ECMHooks", "getPlayerNameWithPrefix", "(Lnet/minecraft/entity/player/EntityPlayerMP;)Ljava/lang/String;", false));
                method.instructions.remove(methodNode);
            }
        }
    }
}
