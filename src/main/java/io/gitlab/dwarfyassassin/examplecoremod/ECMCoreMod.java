package io.gitlab.dwarfyassassin.examplecoremod;

import java.util.Map;
import org.apache.logging.log4j.Logger;
import cpw.mods.fml.relauncher.IFMLLoadingPlugin;

@IFMLLoadingPlugin.TransformerExclusions(value = {"io.gitlab.dwarfyassassin.examplecoremod"})
@IFMLLoadingPlugin.MCVersion(value = "1.7.10")
public class ECMCoreMod implements IFMLLoadingPlugin {

    public static Logger log;

    static {
        System.out.println("Example Core-Mod: Found core mod.");
    }

    @Override
    public String[] getASMTransformerClass() {
        return null;
    }

    @Override
    public String getModContainerClass() {
        return null;
    }

    @Override
    public String getSetupClass() {
        return ECMCoreSetup.class.getName();
    }

    @Override
    public void injectData(Map<String, Object> data) {
    }

    @Override
    public String getAccessTransformerClass() {
        return null;
    }
}
